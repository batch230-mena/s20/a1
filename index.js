//console.log("hello world");

// part 1

let number = Number(prompt("Number please?"));
console.log("The number you provided is " + number + ".");

for(let count = number; count > 0; count--){

	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	}

	if(count%10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(count%5 == 0){
		console.log(count);
	}

}

// part 2

let longWord = "supercalifragilisticexpialidocious";
console.log(longWord);

let consonants = "";

for(let index = 0; index < longWord.length; index++){

	if(longWord[index].toLowerCase() == 'a' || 
		longWord[index].toLowerCase() == 'e' || 
		longWord[index].toLowerCase() == 'i' || 
		longWord[index].toLowerCase() == 'o' || 
		longWord[index].toLowerCase() == 'u'){

		continue;
	}
	else{
		consonants = consonants + longWord[index];
	}
}
console.log(consonants);
